#!/usr/bin/env python3

import socketserver
import socket
import os

class ReusableTCPServer(socketserver.TCPServer):

    allow_reuse_address = True


class MyTCPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        request = self.request.recv(1024)
        request = request.decode('utf-8')
        for line in request.splitlines():
            print('> ' + line)
        response = self.get_response(request)
        for line in response.splitlines():
            print('< ' + line.decode('utf-8', 'replace'))
        print()
        self.request.sendall(response)

    def get_response(self, request):
        request_line = request.splitlines()[0]
        resource = request_line.split()[1]
        script_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(script_path, resource[1:])
        if not os.path.isfile(file_path):
            file_path = os.path.join(script_path, 'responses/404.txt')
        with open(file_path , 'rb') as file:
            response = file.read()
        return response


if __name__ == '__main__':
    server = ReusableTCPServer(('localhost', 8080), MyTCPHandler)
    server.serve_forever()

