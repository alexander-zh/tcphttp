HTTP server to debug headers
============================

This server reads static files in project directory. Instead of normal
HTTP answer it just reads the file and sends it as HTTP answer.
